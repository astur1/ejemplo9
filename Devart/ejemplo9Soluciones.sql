﻿USE ejemplo9;

DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpleados(test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  CREATE OR REPLACE TABLE empleados(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    fechaNacimiento date,
    correo varchar(20),
    edad int,
    PRIMARY KEY (id)
    )ENGINE MYISAM;

  IF(test=1) THEN
    INSERT INTO empleados (nombre, fechaNacimiento)
      VALUES 
      ('Pedro','2000/1/1'),
      ('Ana','1980/10/1'),
      ('Jose','1990/12/4'),
      ('Luis','1985/4/9'),
      ('Ernesto','1975/4/4');
  END IF;

END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpresa (test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  -- crear la tabla
  CREATE OR REPLACE TABLE empresa(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    direccion varchar(20),
    numeroEmpleados int,
    PRIMARY KEY (id)
    )ENGINE MYISAM;
  -- introducir datos de test
  IF(test=1) THEN
    INSERT INTO empresa (nombre, direccion) VALUES 
      ('empresa1','direccion1'),
      ('empresa2','direccion2'),
      ('empresa3','direccion3'),
      ('empresa4','direccion4'),
      ('empresa5','direccion5');
  END IF ;
END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablatrabajan(test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  CREATE OR REPLACE TABLE trabajan(
    id int AUTO_INCREMENT,
    empleado int,
    empresa int,
    fechaInicio date,
    fechaFin date,
    baja bool DEFAULT FALSE,
    PRIMARY KEY(id),
    CONSTRAINT trabajanUK UNIQUE KEY (empleado,empresa)
    )ENGINE MYISAM;

  IF (test=1) THEN 
    INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin) VALUES 
      (1,1,'2015/1/1',NULL);
  END IF;

END //
DELIMITER ;

-- crear el disparador para trabaja

  DELIMITER //
  CREATE OR REPLACE TRIGGER trabajanBI
  BEFORE INSERT
    ON trabajan
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    DECLARE numero1 int DEFAULT 0;

    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

    -- el empleado debe estar en la tabla empleados
    SELECT
      COUNT(*) INTO numero
    FROM empleados
    WHERE id=new.empleado;

    iF (numero = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'el trabajador no está registrado';
    END IF;

    -- la empresa debe existir
    SELECT COUNT(*) INTO numero1
      FROM empresa WHERE id=new.empresa;
    
    iF (numero1 = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;


   --  si la fecha final no esta vacia baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;
  END //
  
  DELIMITER ;

-- realizar los disparadores para actualizar

  DELIMITER //
  CREATE OR REPLACE TRIGGER trabajanBU
  BEFORE UPDATE
    ON trabajan
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    DECLARE numero1 int DEFAULT 0;

    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

   -- el empleado debe estar en la tabla empleados
    SELECT
      COUNT(*) INTO numero
    FROM empleados
    WHERE id=new.empleado;

    iF (numero = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'el trabajador no está registrado';
    END IF;

    -- la empresa debe existir
    SELECT COUNT(*) INTO numero1
      FROM empresa WHERE id=new.empresa;
    
    iF (numero1 = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;

     --  si la fecha final no esta vacia baja pasa a true
    IF (new.fechaFin IS NOT NULL) THEN
      SET new.baja = TRUE;
    ELSE 
      SET new.baja=FALSE;
    END IF;
  END //
  
  DELIMITER ;


-- crear una funcion que reciba como argumento un id de empresa
-- y me devuelva si la empresa existe
  
  DELIMITER //
  CREATE OR REPLACE FUNCTION buscarEmpresa(codigo int)
  RETURNS int 
  BEGIN
    DECLARE valor int DEFAULT 0;

      SELECT COUNT(*) INTO valor
        FROM empresa WHERE id=codigo;
  
  RETURN valor;
  END //
  DELIMITER ;


-- crear una funcion que reciba como argumento un id de empleado
-- y me devuelva si el empleado existe

  DELIMITER //
  CREATE OR REPLACE FUNCTION buscarEmpleado(codigo int)
  RETURNS int 
  BEGIN
    DECLARE valor int DEFAULT 0;

      SELECT COUNT(*) INTO valor
        FROM empleados WHERE id=codigo;
  
  RETURN valor;
  END //
  DELIMITER ;

-- en el disparador de trabaja que ya tenia 
-- voy a intentar utilizar las funciones creadas

  DELIMITER //
  CREATE OR REPLACE TRIGGER trabajanBI
  BEFORE INSERT
    ON trabajan
    FOR EACH ROW
  BEGIN
    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

    -- el empleado debe estar en la tabla empleados

    iF (buscarEmpleado(new.empleado) = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'el trabajador no está registrado';
    END IF;

    -- la empresa debe existir
        
    iF (buscarEmpresa(new.empresa) = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;


   --  si la fecha final no esta vacia baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;
  END //
  
  DELIMITER ;


-- modificamos el disparador de actualizar con las funciones

  DELIMITER //
  CREATE OR REPLACE TRIGGER trabajanBU
  BEFORE UPDATE
    ON trabajan
    FOR EACH ROW
  BEGIN

    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

   -- el empleado debe estar en la tabla empleados

    iF (buscarEmpleado(new.empleado) = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'el trabajador no está registrado';
    END IF;

    -- la empresa debe existir
        
    iF (buscarEmpresa(new.empresa) = 0) THEN
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;

     --  si la fecha final no esta vacia baja pasa a true
    IF (new.fechaFin IS NOT NULL) THEN
      SET new.baja = TRUE;
    ELSE 
      SET new.baja=FALSE;
    END IF;
  END //
  
  DELIMITER ;